# -*- coding: utf-8 -*-
# bla bla bla
if False:
    from gluon import *
    from db import *
    from fleetplus_db import *
    from menu import *


# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - api is an example of Hypermedia API support and access control
#########################################################################

# VIEW FUNCTIONS
@auth.requires_login()
def events():
    # query = fleetplus_db(Events).select(orderby=~Events.id)
    # table = SQLTABLE(query, headers="fieldname:capitalize", _class="table table-condensed table-bordered", truncate=None)
    grid_name = 'Events'
    links = [lambda row: A(SPAN(_class='icon magnifier'),'View Event', _class='button',_title='View  Post',_href=URL("default", "event", args=[row.id]))]
    grid = SQLFORM.grid(fleetplus_db.events, user_signature=False, links_in_grid=True, links=links)

    return locals()

@auth.requires_login()
def passangers():
    response.view = "default/events.html"
    grid_name = 'Passangers'
    grid = SQLFORM.grid(fleetplus_db.passangers, user_signature=False)
    return locals()

@auth.requires_login()
def cars():
    response.view = "default/events.html"
    grid_name = 'Cars'
    grid = SQLFORM.grid(fleetplus_db.cars, user_signature=False)
    # Valami
    return locals()

@auth.requires_login()
def trips():
    response.view = "default/events.html"
    grid_name = 'Trips'
    grid = SQLFORM.grid(fleetplus_db.trips, user_signature=False)
    # Valami 2
    return locals()


@auth.requires_login()
def event():
    eid = request.args(0)
    event = fleetplus_db(Events.id == eid).select().first()
    trips = fleetplus_db((Trips.car_id == Cars.id) & (Trips.event_id == Events.id) & (Events.id == eid)).select()
    passgers = fleetplus_db((Passangers.trip_id == Trips.id) & (Trips.event_id == Events.id) & (Events.id == eid)).count()


    already_driver = False
    if fleetplus_db((Trips.event_id == Events.id) & (Trips.driver_id == Uid) & (Events.id == eid)).select():
        already_driver = True

    already_passanger = False
    if fleetplus_db((Passangers.trip_id == Trips.id) & (Passangers.passanger_id == Uid) & (Trips.event_id == Events.id) & (Events.id == eid)).select():
        already_passanger = True
    if already_passanger:
        reg_driver_btn = B("you are already a passanger, you cannot register a new car!")
    elif already_driver:
        reg_driver_btn = B("you are already a driver, you cannot register a new car!")
    else:
        reg_driver_btn = A("I am also driving!", _href=URL('default', 'trips', args=['new', 'trips']), _class="btn btn-success")

    trips_divs = DIV(reg_driver_btn, _class="row", _id="trips_divs")
    trips_div=''
    # https://localhost/fleetplus/default/trips/new/trips
    
    for i, trip in enumerate(trips):

        passangers = fleetplus_db((Passangers.trip_id == Trips.id) & (Trips.id == trip.trips.id)).select()

        if len(passangers) < trip.cars.passangers:
            full = False
            cls = 'car-available span6'
        else:
            full = True
            cls = 'car-full span6'

        trip_div = DIV('', _class=cls, _id="trip_div")
        # driver
        trip_div.append(
            P(B('Driver: ') + trip.trips.driver_name)
            )
        # car
        trip_div.append(
            P(B('Car: ') + trip.cars.car_name)
            )
        trip_div.append(
            P(B('Capacity: ') + len(passangers) + "/" + trip.cars.passangers)
            )        

        # trip
        trip_div.append(
            P(B('Start address: ') + trip.trips.start_address + "(" + trip.trips.lon + "," + trip.trips.lat + ")")
            )
        trip_div.append(
            P(B('Comments: ') + trip.trips.comments)
            )

        # driver
        driver_of_trip = False
        if str(Uid) == str(trip.trips.driver_id):
            driver_of_trip = True
            rb = A("Changed my mind, not driving!", _href=URL("default", "delete_trip", args=[trip.trips.id]), _class="btn btn-danger")
            eb = A("Edit this trip!", _href=URL("default", "trips", args=["edit", "trips", trip.trips.id]), _class="btn btn-info")
            trip_div.append(rb)
            trip_div.append(eb)

        # passangers
        trip_div.append(P(B('Passangers: ')))
        ps = UL(_id="trip_%s_passangers" % str(trip.trips.id))
        passanger_of_trip = False
        # passanger_addresses = []
        pp = False
        for pa in passangers:
            ps.append(
                LI(
                    str(pa.passangers.passanger_name) + " - " + str(pa.passangers.join_address) + " - " + str(pa.passangers.lon) + " - " + str(pa.passangers.lat),
                    _s_lat=pa.passangers.lon,
                    _s_lon=pa.passangers.lat,
                    _class="trip_map_%s_passangers" % str(trip.trips.id)
                    )
                )
            if str(Uid) == str(pa.passangers.passanger_id):
                passanger_of_trip = pa.passangers.id
            # location = {"A": str(pa.passangers.lon), "F": str(pa.passangers.lat)}
            # passanger_addresses.append({"location": location, "stopover": "true"})
            pp = True
        if pp:
            trip_div.append(ps)
        else:
            trip_div.append(P("No passangers yet :)"))
        if already_driver:
            trip_div.append("you already drive, cannot join car")
        else:
            if already_passanger:
                if passanger_of_trip:
                    rb = A("Leave this trip", _href=URL("default", "leave_trip", args=[trip.trips.id, passanger_of_trip]), _class="btn")
                    trip_div.append(rb)
                else:
                    trip_div.append(B("you already joined another car!"))
            elif full:
                trip_div.append(B("the trip is full!"))
            else:
                trip_div.append(
                    A(
                        "I am joining you at the starting point",
                        _href=URL("default", "join_trip", args=[trip.trips.id, Uid, trip.trips.lon, trip.trips.lat]),
                        _class="btn"))
                trip_div.append(
                    A(
                        "Let me pick a point to join",
                        _href=URL("default", "pick_location", args=[trip.trips.id, Uid]),
                        _class="btn"))

        map_div = DIV(
            'trip_map',
            _s_lat=trip.trips.lon,
            _s_lon=trip.trips.lat,
            _class="trip_map",
            _id="trip_map_%s" % trip.trips.id,
            # _passangers=passanger_addresses,
            _style="width: 500px; height: 500px;")

        
        trip_div.append(map_div)
        trip_div.append(HR())
        if i % 2 == 0:
            trips_div = DIV(_class="row", _id="trips_div")
            trips_div.append(trip_div)
        else:
            trips_div.append(trip_div)
            trips_divs.append(trips_div)
            trips_div = DIV(_class="row", _id="trips_div")
        # if i == len(trips) and i % 2 == 1:
    trips_divs.append(trips_div)

            
            


    # cars
    return locals()

# SUPpORT FUNCTIONS
# def join_trip(trip_id, uid, lon, lat):
@auth.requires_login()
def join_trip():
    nm = Ulname + " " + Ufname
    a = None
    if request.vars["join_address"]:
        a = request.vars["join_address"]
    fleetplus_db['passangers'].insert(trip_id=request.args(0), passanger_id=request.args(1), lon=request.args(2), lat=request.args(3), passanger_name=nm, join_address=a)
    session.flash = "Joined trip at starting point"
    e = fleetplus_db(Trips.id == request.args(0)).select().first()['event_id']
    redirect(URL('default', "event", args=[e]))
    return

# def leave_trip(trip_id, passanger_id):
@auth.requires_login()
def leave_trip():
    session.flash = "Trip left"
    # fleetplus_db(Passangers.id == passanger_id).delete()
    # Hello bello
    fleetplus_db(Passangers.id == request.args(1)).delete()
    e = fleetplus_db(Trips.id == request.args(0)).select().first()['event_id']
    redirect(URL('default', "event", args=[e]))


@auth.requires_login()
def delete_trip():
    session.flash = "Deleted car and passangers"
    # fleetplus_db(Passangers.id == passanger_id).delete()
    e = fleetplus_db(Trips.id == request.args(0)).select().first()['event_id']
    fleetplus_db(Trips.id == request.args(0)).delete()
    fleetplus_db(Passangers.trip_id == request.args(0)).delete()
    redirect(URL('default', "event", args=[e]))
    pass

@auth.requires_login()
def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """

    return dict()

@auth.requires_login()
def pick_location():
    trip_id = request.args(0)
    trip_row = fleetplus_db(Trips.id == trip_id).select().first()
    car_row = fleetplus_db(Cars.id == trip_row.car_id).select().first()
    event_row = fleetplus_db(Events.id == trip_row.event_id).select().first()
    passangers = fleetplus_db(Passangers.trip_id == trip_id).select()
    s_lon = trip_row.lon
    s_lat = trip_row.lat
    e_lon = event_row.lon
    e_lat = event_row.lat
    wpoint_ul = UL(_hidden=True)
    for p in passangers:
        wpoint_ul.append(LI(
            p.passanger_name, _s_lon=p.lon, _s_lat=p.lat, _class="passangers"))
    return locals()


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_login()
def api():
    """
    this is example of API with access control
    WEB2PY provides Hypermedia API (Collection+JSON) Experimental
    """
    from gluon.contrib.hypermedia import Collection
    rules = {
        '<tablename>': {'GET':{},'POST':{},'PUT':{},'DELETE':{}},
        }
    return Collection(db).process(request,response,rules)
