# -*- coding: utf-8 -*-
import datetime
import time
from gluon import *
from md5 import md5

now = Now = datetime.datetime.now()
CurrentDate = datetime.date(
    datetime.datetime.now().year,
    datetime.datetime.now().month,
    datetime.datetime.now().day)
CurrentDay = datetime.datetime.now().day
CurrentMonth = datetime.datetime.now().month
CurrentYear = datetime.datetime.now().year
OneHourEarly = (
    datetime.datetime.fromtimestamp(time.time()) -
    datetime.timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S')


def date_widget(f, v):
    wrapper = DIV()
    inp = SQLFORM.widgets.string.widget(f, v, _class="jqdate")
    jqscr = SCRIPT(
        """jQuery(document).ready(
        function(){jQuery('#%s').datepicker({dateFormat:'yy.mm.dd'});});"""
        % inp['_id'], _type="text/javascript")
    wrapper.components.extend([inp, jqscr])
    return wrapper


# dsscentral_db = DAL('mysql://isdash:Passw0rd@localhost/dss_central_dev')
fleetplus_db = DAL(settings.database_uri)
# dsstrainingcat = DAL(settings.training_database_uri)

if auth.is_logged_in():
    Uid = auth.user.id
    Ufname = auth.user.first_name
    Ulname = auth.user.last_name
else:
    Uid = None

signature = fleetplus_db.Table(
    fleetplus_db,
    'signature',
    Field('created_on', 'datetime', default=request.now),
    Field('created_by', default=Uid),
    Field('modified_on', 'datetime', update=request.now),
    Field('modified_by', update=Uid))

signature.created_on.readable = False
signature.created_by.readable = False
signature.modified_by.readable = False
signature.modified_on.readable = False
signature.created_on.writable = False
signature.created_by.writable = False
signature.modified_by.writable = False
signature.modified_on.writable = False

Cars = fleetplus_db.define_table(
    'cars',
    Field(
        'car_name',
        'string',
        length=50
        ),
    Field(
        'licence',
        'string',
        length=50
        ),
    Field(
        'passangers',
        'integer'
        ),
    Field(
        'is_active',
        'boolean',
        writable=False,
        readable=False,
        default=True),
    signature,
    format='%(car_name)s (%(id)s)')
Cars._enable_record_versioning(
    archive_db=fleetplus_db,
    archive_name='cars_archive',
    current_record='current_record',
    is_active='is_active')

Events = fleetplus_db.define_table(
    'events',
    Field(
        'event_name',
        'string',
        length=255
        ),
    Field(
        'location',
        'string',
        length=255
        ),
    Field(
        'start_datetime',
        'datetime'
        ),
    Field(
        'agenda',
        'text',
        ), 
    Field(
        'lon',
        'decimal(10,6)',
        ), 
    Field(
        'lat',
        'decimal(10,6)',
        ),    
    Field(
        'is_active',
        'boolean',
        writable=False,
        readable=False,
        default=True),
    signature,
    format='%(event_name)s (%(id)s)')
Events._enable_record_versioning(
    archive_db=fleetplus_db,
    archive_name='events_archive',
    current_record='current_record',
    is_active='is_active')

Trips = fleetplus_db.define_table(
    'trips',
    Field(
        'car_id',
        fleetplus_db.cars
        ),
    Field(
        'event_id',
        fleetplus_db.events
        ),
    Field(
        'driver_name',
        'string'
        ),
    Field(
        'driver_id',
        'integer',
        requires=IS_EMPTY_OR(IS_IN_DB(db, 'auth_user.id', db.auth_user._format))
        ),
    Field(
        'start_datetime',
        'datetime'
        ),
    Field(
        'start_address',
        'string'
        ),
    Field(
        'lon',
        'decimal(10,6)',
        ), 
    Field(
        'lat',
        'decimal(10,6)',
        ),
    Field(
        'comments',
        'text',
        ),    
    Field(
        'is_active',
        'boolean',
        writable=False,
        readable=False,
        default=True),
    signature,
    format='%(driver_name)s (%(id)s)')
Trips._enable_record_versioning(
    archive_db=fleetplus_db,
    archive_name='trips_archive',
    current_record='current_record',
    is_active='is_active')

Passangers = fleetplus_db.define_table(
    'passangers',
    Field(
        'trip_id',
        fleetplus_db.trips
        ),
    Field(
        'passanger_name',
        'string'
        ),
    Field(
        'passanger_id',
        'integer',
        requires=IS_EMPTY_OR(IS_IN_DB(db, 'auth_user.id', db.auth_user._format))
        ),
    Field(
        'start_datetime',
        'datetime'
        ),
    Field(
        'join_type',
        'string'
        ),
    Field(
        'join_address',
        'string'
        ),
    Field(
        'lon',
        'decimal(10,6)',
        ), 
    Field(
        'lat',
        'decimal(10,6)',
        ),
    Field(
        'status',
        'string'
        ),
    Field(
        'comments',
        'text',
        ),    
    Field(
        'is_active',
        'boolean',
        writable=False,
        readable=False,
        default=True),
    signature,
    format='%(passanger_name)s (%(id)s)'
    )
Passangers._enable_record_versioning(
    archive_db=fleetplus_db,
    archive_name='passangers_archive',
    current_record='current_record',
    is_active='is_active')








# Departments = dsscentral_db.define_table(
#     'departments',
#     Field('id', unique=True),
#     Field(
#         'dep_name',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='nameinput')),
#     Field(
#         'url_dep',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='urlinput')),
#     Field('ad_dep', 'string', length=50),
#     Field('status', 'string', length=50),
#     Field('comp_name', 'string', length=50),
#     Field('leader_id', 'integer', length=11),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature,
#     format='%(id)s (%(dep_name)s)')
# Departments.url_dep.requires = IS_ALPHANUMERIC(
#     error_message='must be alphanumeric!')
# Departments._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='departments_archive',
#     current_record='current_record',
#     is_active='is_active')

# Sdus = dsscentral_db.define_table(
#     'sdus',
#     Field('id', unique=True),
#     Field(
#         'sdu_name',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='nameinput')),
#     Field(
#         'url_sdu',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='urlinput')),
#     Field('ad_sdu', 'string', length=50),
#     Field('old_name', 'string', length=50),
#     Field('status', 'string', length=50),
#     Field('department_id', dsscentral_db.departments),
#     Field('leader_id', 'integer', length=11),
#     Field('modified_data', 'string'),
#     Field('distribution_list', 'string', length=255),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature,
#     format='%(sdu_name)s (%(id)s)')
# Sdus.url_sdu.requires = IS_ALPHANUMERIC(error_message='must be alphanumeric!')
# Sdus.status.requires = IS_IN_SET(['active', 'inactive'])
# Sdus.status.default = 'active'
# Sdus.sdu_name.requires = IS_NOT_IN_DB(dsscentral_db, Sdus.sdu_name)
# Sdus.modified_data.readable = Sdus.modified_data.writable = False
# Sdus._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='sdus_archive',
#     current_record='current_record',
#     is_active='is_active')

# Groups = dsscentral_db.define_table(
#     'groups',
#     Field('id', unique=True),
#     Field(
#         'group_name',
#         'string',
#         length=255,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='nameinput')),
#     Field(
#         'url_group',
#         'string',
#         length=255,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='urlinput')),
#     Field('old_name', 'string', length=50),
#     Field('status', 'string', length=255),
#     Field('leader_id', 'integer', length=11),
#     Field('team_logo', 'upload'),
#     Field('email_sending', 'boolean', default=True),
#     Field('setup_group', 'boolean', default=False),
#     Field('description', 'text'),
#     Field('default_sdu', dsscentral_db.sdus),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature,
#     format='%(group_name)s (%(id)s)')
# Groups.status.requires = IS_IN_SET(['active', 'cancelled', 'transition'])
# Groups.group_name.requires = IS_NOT_IN_DB(dsscentral_db, Groups.group_name)
# Groups._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='groups_archive',
#     current_record='current_record',
#     is_active='is_active')

# CentralAttributes = dsscentral_db.define_table(
#     'central_attributes',
#     Field('id', unique=True),
#     Field(
#         'attr_name',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='nameinput')),
#     Field(
#         'url_attr',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='urlinput')),
#     Field('description', 'text'),
#     Field('status', 'string', length=50),
#     Field('group_id', 'integer', length=11),
#     Field('attr_type', 'string', length=50),
#     Field('is_default', 'string', length=50),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)
# CentralAttributes.url_attr.requires = IS_ALPHANUMERIC(
#     error_message='must be alphanumeric!')
# CentralAttributes._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='central_attributes_archive',
#     current_record='current_record',
#     is_active='is_active')

# GroupAttributes = dsscentral_db.define_table(
#     'group_attributes',
#     Field('id', unique=True),
#     Field(
#         'attr_name',
#         'string',
#         label=T("Attribute Name"),
#         comment=T("The name of the attribute, as used within the team"),
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='nameinput')),
#     Field(
#         'url_attr',
#         'string',
#         label=T("URL Name"),
#         length=50,
#         comment=T("The name used in links, automatically generated"),
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='urlinput')),
#     Field(
#         'description',
#         'text',
#         label=T("Description"),
#         comment=T("Describe what this attribute is, what it is used for")),
#     Field('status', 'string', length=50, label=T("Status")),
#     Field(
#         'type',
#         'string',
#         length=50,
#         label=T("Type"),
#         comment=T("""Use the most appropriate type! Account: unique user account that has a username; Access rights: additional rights for existing accounts""")),
#     Field('group_id', dsscentral_db.groups),
#     Field(
#         'example_user',
#         'string',
#         label=T('Example User'),
#         comment=T("""If you have an example user when ordering an account, access right, you can document it here""")),
#     Field(
#         'order_instructions',
#         'text',
#         label=T("Order Instructions"),
#         comment=T("""Write down the detailed ordering instructions to aid you and your deputies with the details""")),
#     Field(
#         'order_template',
#         'upload',
#         label=T("Order Template"),
#         comment=T("""You can upload a template (for example an email or form) that is used to acquire the attribute""")),
#     Field(
#         'cost',
#         'double',
#         label=T("Cost"),
#         comment=T("Cost of the attribute")),
#     Field(
#         'is_default',
#         'boolean',
#         default=False,
#         label="Is Default for all Roles?"),
#     Field('is_central', 'boolean', default=False),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature,
#     format='%(id)s (%(attr_name)s)')
# GroupAttributes.url_attr.requires = IS_ALPHANUMERIC(
#     error_message='must be alphanumeric!')
# # GroupAttributes.attr_name.requires = IS_NOT_IN_DB(
# #     dsscentral_db, 'group_attributes.attr_name')
# GroupAttributes.attr_name.requires = IS_NOT_EMPTY()
# GroupAttributes.status.requires = IS_IN_SET(['active', 'cancelled'])
# GroupAttributes.status.default = "active"
# # GroupAttributes.status.readable = GroupAttributes.status.writable = False
# GroupAttributes.is_default.readable = False
# GroupAttributes.is_default.writable = False
# GroupAttributes.type.requires = IS_IN_SET(sorted(settings.attribute_types))
# GroupAttributes._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='group_attributes_archive',
#     current_record='current_record',
#     is_active='is_active')

# GroupRoles = dsscentral_db.define_table(
#     'group_roles',
#     Field('id', unique=True),
#     Field(
#         'role_name',
#         'string',
#         length=50,
#         label=T("Role Name"),
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='nameinput')),
#     Field(
#         'url_role',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='urlinput')),
#     Field('description', 'text'),
#     Field('status', 'string', length=50),
#     Field('group_id', dsscentral_db.groups),
#     Field('example_user', 'string'),
#     Field('role_type', 'string', length=50),
#     Field('skills', 'list:string'),
#     Field('languages', 'list:string'),
#     Field('experience', 'string'),
#     Field('career_path', 'string'),
#     Field('job_matrix', 'string'),
#     Field('dep', 'string'),
#     Field(
#         'is_central',
#         'boolean', writable=False, readable=False, default=False),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)
# GroupRoles.url_role.requires = IS_ALPHANUMERIC(
#     error_message='must be alphanumeric!')
# GroupRoles.status.requires = IS_IN_SET(['active', 'cancelled', 'not in use'])
# GroupRoles.role_type.requires = IS_IN_SET(['overhead', 'productive'])
# GroupRoles.role_name.requires = IS_NOT_EMPTY()
# GroupRoles.role_name.requires = IS_NOT_IN_DB(
#     dsscentral_db, GroupRoles.role_name)
# GroupRoles._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='group_roles_archive',
#     current_record='current_record',
#     is_active='is_active')

# GroupSkills = dsscentral_db.define_table(
#     'group_skills',
#     Field('id', unique=True),
#     Field(
#         'skill_name',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='nameinput')),
#     Field(
#         'url_skill',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='urlinput')),
#     Field('description', 'text'),
#     Field('credits', 'integer'),
#     Field('status', 'string', length=50),
#     Field('training_link', 'string'),
#     Field('group_id', dsscentral_db.groups),
#     Field('type', 'string', length=50),
#     Field('skill_code', 'string', length=50),
#     Field('skill_name_hu', 'string', length=50),
#     Field('skill_name_ger', 'string', length=50),
#     Field(
#         'is_default',
#         'boolean',
#         default=False,
#         label="Is default for all users?"),
#     Field('is_central', 'boolean', default=False),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)
# GroupSkills.url_skill.requires = IS_ALPHANUMERIC(
#     error_message='must be alphanumeric!')
# GroupSkills.status.requires = IS_IN_SET(['active', 'cancelled', 'not in use'])
# GroupSkills.type.requires = IS_IN_SET(settings.skill_types)
# GroupSkills.credits.requires = IS_NOT_EMPTY()
# GroupSkills.credits.widget = lambda f, v: SQLFORM.widgets.string.widget(
#     f, v, _placeholder="Required", _class="integer required")
# # GroupSkills.skill_code.readable = GroupSkills.skill_code.writable = False
# GroupSkills.training_link.readable = GroupSkills.training_link.writable = False
# GroupSkills._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='group_skills_archive',
#     current_record='current_record',
#     is_active='is_active')

# RoleAttributes = dsscentral_db.define_table(
#     'role_attributes',
#     Field('id', unique=True),
#     Field('role_id', dsscentral_db.group_roles),
#     Field('attr_id', dsscentral_db.group_attributes),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)
# RoleAttributes._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='role_attributes_archive',
#     current_record='current_record',
#     is_active='is_active')

# RoleSkills = dsscentral_db.define_table(
#     'role_skills',
#     Field('id', unique=True),
#     Field('role_id', dsscentral_db.group_roles),
#     Field('skill_id', dsscentral_db.group_skills),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)
# RoleSkills._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='role_skills_archive',
#     current_record='current_record',
#     is_active='is_active')

# Teams = dsscentral_db.define_table(
#     'teams',
#     Field('id', unique=True),
#     Field(
#         'team_name',
#         'string',
#         length=50,
#         label=T("Team Name"),
#         comment=T("The official team name as set by HR; "),
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='nameinput')),
#     Field(
#         'url_team',
#         'string',
#         length=50,
#         widget=lambda field, value: SQLFORM.widgets.string.widget(
#             field, value, _id='urlinput')),
#     Field(
#         'ad_team',
#         'string',
#         length=50,
#         label=T("Org Unit"),
#         comment=T("""The official name of the team as set by HR; this is the 'Department' field in the ITSH Phonebook""")),
#     Field('old_name', 'string', length=50),
#     Field('status', 'string', length=50),
#     # Field('group_id', dsscentral_db.groups),
#     Field('sdu_id', dsscentral_db.sdus),
#     Field('default_group'),
#     Field(
#         'leader_id',
#         'integer',
#         length=11,
#         label=T("Leader"),
#         comment=T(
#             "The (team)leader who is directly responsible for the Team"),),
#     Field(
#         'location',
#         'string',
#         length=50,
#         label=T("Location"),
#         comment=T("The location where the team is based"),),
#     Field(
#         'cost_center',
#         'string',
#         length=50,
#         label=T("Cost Center"),
#         comment=T("The unique cost center of the team")),
#     Field(
#         'distribution_list',
#         'string',
#         length=50,
#         label=T("Distribution List"),
#         comment=T("The email address of the team's distribution list"),),
#     Field('modified_data', 'string'),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature,
#     format='%(team_name)s (%(id)s)')
# Teams.location.requires = IS_IN_SET(
#     ['Budapest', 'Debrecen', 'Kecskemét', 'Pécs', 'Other'])
# Teams.cost_center.requires = IS_NOT_IN_DB(dsscentral_db, Teams.cost_center)
# # Teams.url_team.requires = IS_ALPHANUMERIC(
# #     error_message='must be alphanumeric!')
# # Teams.status.requires = IS_IN_SET(['active', 'cancelled', 'transition'])
# Teams.status.default = 'active'
# Teams.modified_data.readable = Teams.modified_data.writable = False
# Teams.status.readable = Teams.status.writable = False
# Teams.team_name.requires = IS_NOT_IN_DB(dsscentral_db, Teams.team_name)
# Teams.default_group.requires = IS_EMPTY_OR(
#     IS_IN_DB(dsscentral_db, Groups.id, '%(group_name)s (%(id)s)'))
# Teams.distribution_list.requires = IS_EMPTY_OR(
#     IS_EMAIL(error_message=T('Please insert valid email address!')))
# Teams._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='teams_archive',
#     current_record='current_record',
#     is_active='is_active')

# GroupTeams = dsscentral_db.define_table(
#     'group_teams',
#     Field('id', unique=True),
#     Field('group_id', dsscentral_db.groups),
#     Field('team_id', dsscentral_db.teams),
#     Field('description', 'text'),
#     signature
#     # primarykey=['group_id', 'team_id']
#     # Field( 'user_uid_md5',
#     #     length=32,
#     #     unique=True,
#     #     writable=False,
#     #     readable=False,
#     #     compute=lambda row: md5(
#     #     "{0}{1}".format(row.group_id,row.team_id)).hexdigest())
#     )
# # GroupTeams.team_id.requires=IS_NOT_IN_DB(
# #     dsscentral_db(GroupTeams.group_id==request.vars.group_id),
# #    'group_teams.team_id')

# Employees = dsscentral_db.define_table(
#     'employees',
#     Field(
#         'id',
#         unique=True,
#         label="#"),
#     Field(
#         'emp_name',
#         'string',
#         length=50,
#         label=T("Employee Name"),
#         # comment=T("The Name (UNS) field from the ITSH Phonebook")
#         ),
#     Field(
#         'tseu_name',
#         'string',
#         length=50),
#     Field(
#         'emp_number',
#         'string',
#         length=50,
#         label="WIW ID",
#         # comment=T("""The WIW ID field from the ITSH Phonebook
#         #    (also called employee number)""")
#         ),
#     Field(
#         'uns_email',
#         'string',
#         length=100),
#     Field(
#         'tseu_email',
#         'string',
#         length=100,
#         label="TS-EU e-mail",
#         requires=CLEANUP()),
#     Field(
#         'uns_uid',
#         'string',
#         length=50,
#         label=T("UNS ID"),
#         # comment=T("The Account name (UNS) field from the ITSH Phonebook")
#         ),
#     Field(
#         'tseu_uid',
#         'string',
#         length=50,
#         label="TS-EU ID",
#         # comment=T("The Account name (TS-EU) field from the ITSH Phonebook")
#         ),
#     Field(
#         'job_desc',
#         'string',
#         length=50,
#         label=T("Job Title"),
#         # comment=T("The Job title field from the ITSH Phonebook")
#         ),
#     Field(
#         'status',
#         'string',
#         length=50,
#         label="Status"),
#     Field(
#         'team_id',
#         dsscentral_db.teams),
#     Field(
#         'emp_image',
#         'upload'),
#     Field(
#         'emp_level',
#         'string',
#         length=50,
#         label=T("Experience Level")),
#     Field(
#         'flag',
#         'string',
#         length=255),
#     Field(
#         'ad_dep',
#         'string',
#         length=100,
#         label='Active Directory Departmnet'),
#     Field(
#         'transfer_to',
#         'string',
#         length=100),
#     Field(
#         'start_date',
#         'date'),
#     Field(
#         'productive_date',
#         'date'),
#     Field(
#         'end_date',
#         'date'),
#     Field(
#         'transfer_date',
#         'date'),
#     Field(
#         'passive_date',
#         'date'),
#     Field(
#         'employee_amount',
#         'string',
#         requires=IS_IN_SET(settings.fte_amounts),
#         label=T('FTE Ratio'),
#         default='1.0'),
#     Field(
#         'old_name',
#         'string',
#         length=50,
#         label=T("Old Name"),
#         # comment=T("The Name (UNS) field from the ITSH Phonebook")
#         ),
#     Field(
#         'sync_status',
#         'string'),
#     Field(
#         'sync_time',
#         'datetime',
#         default=request.now,
#         update=request.now),
#     Field(
#         'exit_reason',
#         'string'),
#     Field(
#         'exit_comment',
#         'text'),
#     Field(
#         'modified_data',
#         'string'),
#     Field(
#         'is_passive',
#         'boolean',
#         default=False),
#     Field(
#         'ignore_sync',
#         'boolean',
#         default=False),
#     Field(
#         'external',
#         'boolean',
#         default=False),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature,
#     format='%(emp_name)s (%(id)s)')
# exit_reasons_list = ['exit ITSH', 'within ITSH', 'passive']
# Employees.tseu_name.readable = False
# Employees.uns_email.writable = Employees.uns_email.readable = False
# Employees.emp_image.writable = Employees.emp_image.readable = False
# Employees.ad_dep.writable = Employees.ad_dep.readable = False
# Employees.transfer_to.writable = False
# Employees.flag.readable = Employees.flag.writable = False
# Employees.external.readable = Employees.external.writable = False
# # Employees.uns_uid.requires = IS_NOT_EMPTY()
# Employees.emp_name.requires = IS_NOT_EMPTY()
# Employees.emp_number.requires = IS_NOT_EMPTY()
# Employees.start_date.requires = IS_NOT_EMPTY()
# Employees.job_desc.requires = IS_NOT_EMPTY()
# # Employees.transfer_date.readable = False
# # Employees.transfer_date.writable = False
# Employees.status.requires = IS_EMPTY_OR(IS_IN_SET(
#     ['active', 'passive', 'leaver', 'trainee', 'transfer']))
# Employees.emp_level.requires = IS_EMPTY_OR(IS_IN_SET(aas.levels))
# Employees.emp_level.default = 'junior_1'
# Employees.tseu_uid.requires = IS_NOT_IN_DB(dsscentral_db, Employees.tseu_uid)
# Employees.emp_number.requires = [
#     IS_NOT_IN_DB(dsscentral_db, Employees.emp_number),
#     IS_MATCH(
#         '^(00)(\d{6})$',
#         error_message="Use the follwoing format: 00123456")]
# Employees.exit_reason.requires = IS_EMPTY_OR(IS_IN_SET(exit_reasons_list))
# Employees.status.default = 'active'
# Employees.external.default = 'F'
# dsscentral_db.employees._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='employees_archive',
#     current_record='current_record',
#     is_active='is_active')


# EmpAttributes = dsscentral_db.define_table(
#     'emp_attributes',
#     Field(
#         'id',
#         unique=True),
#     Field(
#         'emp_id',
#         dsscentral_db.employees),
#     Field(
#         'attr_id',
#         dsscentral_db.group_attributes),
#     Field(
#         'attr_value',
#         'string',
#         default=""),
#     Field(
#         'status',
#         'string',
#         requires=IS_IN_SET(["Rendelendő", "Megrendelve", "Kész"]),
#         default="Rendelendő"),
#     Field(
#         'comment',
#         'text'),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)
# EmpAttributes.attr_value.default = ""
# dsscentral_db.emp_attributes._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='emp_attributes_archive',
#     current_record='current_record',
#     is_active='is_active')


# EmpSkills = dsscentral_db.define_table(
#     'emp_skills',
#     Field('id', unique=True),
#     Field('emp_id', dsscentral_db.employees),
#     Field('skill_id', dsscentral_db.group_skills),
#     Field('skill_value', 'string', default=""),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)
# dsscentral_db.emp_skills._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='emp_skills_archive',
#     current_record='current_record',
#     is_active='is_active')

# EmpRoles = dsscentral_db.define_table(
#     'emp_roles',
#     Field('id', unique=True),
#     Field('emp_id', dsscentral_db.employees),
#     Field('role_id', dsscentral_db.group_roles),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)
# dsscentral_db.emp_roles._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='emp_roles_archive',
#     current_record='current_record',
#     is_active='is_active')

# AttributeDocumentation = dsscentral_db.define_table(
#     'attribute_documentation',
#     Field('id', unique=True),
#     Field('attr_id', dsscentral_db.emp_attributes),
#     Field('date_requested', 'date'),
#     Field('date_receive', 'date'),
#     Field('date_deleted', 'date'),
#     Field('comment', 'text'),
#     Field('request_ticket_number', 'string', length=30),
#     Field('delete_ticket_number', 'string', length=30),
#     Field('request_document', 'upload', uploadfield='request_document_data'),
#     Field('request_document_data', 'blob', default=''),
#     Field(
#         'delete_request_document',
#         'upload',
#         uploadfield='delete_request_document_data'),
#     Field('delete_request_document_data', 'blob', default=''),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)
# AttributeDocumentation._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='attribute_documentation_archive',
#     current_record='current_record',
#     is_active='is_active')


# LdapData = dsscentral_db.define_table(
#     'ldap_data',
#     Field('emp_name', 'string'),
#     Field('emp_number', 'string'),
#     Field('job_desc', 'string'),
#     Field('leader', 'string'),
#     Field('uns_email', 'string'),
#     Field('uns_uid', 'string'),
#     Field('ad_dep', 'string'))

# EmployeesChanges = dsscentral_db.define_table(
#     'employees_log_table',
#     Field('id', readable=False, writable=False, unique=True),
#     Field('uns_id', 'string', length=100),
#     Field('tseu_id', 'string', length=100),
#     Field('old_data', 'text'),
#     Field('new_data', 'text'),
#     Field('date_logged', 'datetime', default=now))


# AvayaStations = dsscentral_db.define_table(
#     'avaya_stations',
#     Field(
#         'station_id_number',
#         'string',
#         length=50,
#         label="Station ID",
#         comment=T("The Station ID number in the format XXX-XXXX (for example: 361-1000)")),
#     Field(
#         'asset_number',
#         'string',
#         length=50,
#         label=T("Asset Number"),
#         comment=T("The ITSH Asset number the Station ID is activated on or if used on a softphone, then write 'softphone'")),
#     Field(
#         'comment',
#         'text',
#         label=T("Comment")),
#     Field(
#         'status',
#         'string',
#         length=50),
#     Field(
#         'delete_request_date',
#         'date',
#         label=T("Delete Request Date"),
#         comment=T("The date the deletion was requested; If a Station ID is no longer needed (the user left and the Station ID is not re-assigned) a Delete request has to be raised through iSD Help")),
#     Field(
#         'delete_ticket',
#         'string',
#         length=255,
#         label=T("Delete Ticket"),
#         comment=T("The ticket number from iSD Help for the Delete request")),
#     Field(
#         'huger',
#         'string',
#         length=50,
#         label="HU/GER"),
#     Field(
#         'emp_id',
#         dsscentral_db.employees),
#     Field(
#         'is_active',
#         'boolean',
#         writable=False,
#         readable=False,
#         default=True),
#     signature)

# AvayaStations.station_id_number.requires = [
#     IS_NOT_EMPTY(),
#     IS_NOT_IN_DB(dsscentral_db, AvayaStations.station_id_number)]
# AvayaStations.asset_number.requires = IS_NOT_EMPTY()
# AvayaStations.asset_number.widget = lambda f, v: SQLFORM.widgets.string.widget(
#     f, v, _placeholder="Required", _class="string required")
# AvayaStations._enable_record_versioning(
#     archive_db=dsscentral_db,
#     archive_name='avaya_stations_archive',
#     current_record='current_record',
#     is_active='is_active')


# AvayaIsdReport = dsscentral_db.define_table(
#     'avaya_isd_report',
#     Field("Location_id", "string", label="Location Id"),
#     Field("Station", "string", label="Station"),
#     Field("Ext", "string", label="Extension"),
#     Field("Type", "string", label="Type"),
#     Field("Port", "string"),
#     Field("Name", "string"),
#     Field("Data_Ext", "string"),
#     Field("Cover_1", "string"),
#     Field("Cover_2", "string"),
#     Field("COS", "string"),
#     Field("COR", "string"),
#     Field("TN", "string"),
#     Field("Room", "string"),
#     Field("Jack", "string"),
#     Field("Cable", "string"),
#     Field("Survivable_GK_Node_Name", "string"),
#     Field("Cluster", "string"),
#     Field("Location_subdivided", "string"),
#     Field("Station_ID_relevant", "string"),
#     Field("Location", "string"),
#     Field("Status", "string"))

# EmpLanguages = dsscentral_db.define_table(
#     'emp_languages',
#     Field("emp_id", dsscentral_db.employees),
#     Field("language", "string"),
#     Field("level"),
#     Field("comment", "string"),
#     Field("is_service_language", "boolean"),
#     signature)
# EmpLanguages.level.requires = IS_IN_SET(settings.language_levels)
# EmpLanguages.language.requires = IS_IN_SET(settings.service_languages)

# Departments.leader_id.requires = IS_EMPTY_OR(
#     IS_IN_DB(dsscentral_db, Employees))
# Teams.leader_id.requires = IS_EMPTY_OR(
#     IS_IN_DB(dsscentral_db, Employees, "%(emp_name)s (%(id)s)"))
# Groups.leader_id.requires = IS_EMPTY_OR(
#     IS_IN_DB(dsscentral_db, Employees, "%(emp_name)s (%(id)s)"))
# Sdus.leader_id.requires = IS_EMPTY_OR(
#     IS_IN_DB(dsscentral_db, Employees, "%(emp_name)s (%(id)s)"))
