var directionsDisplay = new google.maps.DirectionsRenderer();
var map;
var directionsService = new google.maps.DirectionsService();
var markers = [];
var infoParkLatLng = new google.maps.LatLng(47.470047, 19.06042);
var tudasParkLatLng = new google.maps.LatLng(47.5559415, 21.5871233)


function initialize() {

    // Map options
    var mapOptions = {
        zoom: 13,
        center: infoParkLatLng};
        map = new google.maps.Map(
            document.getElementById('map-canvas'),
            mapOptions);

    // Initial marker for infopark
    /*var marker = new google.maps.Marker({
        position: infoParkLatLng,
        map: map,
        draggable:true,
        title: "Infopark Budapest",
        clickable: false,
    });*/
    addMarker(infoParkLatLng);

    /* Search input */
    var input = (document.getElementById('map-search'))
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();

    // Calculate Route button
    var calcRouteButton = document.getElementById('calcroute-button');
    // console.log(calcRouteButton);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(calcRouteButton);
    google.maps.event.addDomListener(calcRouteButton, 'click', function() {calcRoute(infoParkLatLng, tudasParkLatLng)});

    // Clear Route button
    var clearRouteButton = document.getElementById('clearroute-button');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(clearRouteButton);
    google.maps.event.addDomListener(clearRouteButton, 'click', function() {clearRoute()});

    //** Event listeners **//
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        infowindow.close();
        var place = autocomplete.getPlace();
        // If no geometry
        if (!place.geometry) {
            window.alert("Cannot find place");
        }
        // if place has geometry
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        }
        else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        /*marker.setIcon(({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
           }));*/
        var marker = addMarker(place.geometry.location);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open(map, marker);
    }); // end of autocompletelistener


    /* Event called when marker clicked
    google.maps.event.addListener(marker, 'click', function() {
        calcRoute(marker.position);
    }); */

    /* Event called on drag stop
    google.maps.event.addListener(marker, 'dragend', function() {
        console.log(marker.position);
        calcRoute(marker.position);
    });*/

    google.maps.event.addListener(map, 'click', function(event) {
        addMarker(event.latLng);
    });

    directionsDisplay.setMap(map);

}


/* Function to calculate route */
function calcRoute(sp, ep) {
    var wpoints = []
    // console.log(google.maps);
    for (var i = markers.length - 1; i >= 0; i--) {
        // console.log(markers[i].position);
        wpoints.push({location: markers[i].position, stopover: true});
    };
    // console.log(wpoints)
    var start = sp;
    var end = ep;
    // var end = wpoints[wpoints.length - 1];
    var request = {
        origin: start,
        destination: end,
        waypoints: wpoints,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });
}

function addMarker(location) {
    // console.log(location);
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        draggable:true,
        title: "",
        clickable: false,
    });
    markers.push(marker);
    return marker;
}


function clearRoute() {
    // console.log(map);
    for (var i = markers.length - 1; i >= 0; i--) {
        markers[i].setMap(null);
    };
    markers = [];
    directionsDisplay.setMap(null);
    addMarker(infoParkLatLng);
}

google.maps.event.addDomListener(window, 'load', initialize);
